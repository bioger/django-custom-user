#!/usr/bin/env python
from setuptools import setup, find_packages
from os import path
import codecs
import os
import re
import sys

def read(*parts):
    file_path = path.join(path.dirname(__file__), *parts)
    return codecs.open(file_path, encoding='utf-8').read()


def find_version(*parts):
    version_file = read(*parts)
    version_match = re.search(r"^__version__ = ['\"]([^'\"]*)['\"]", version_file, re.M)
    if version_match:
        return str(version_match.group(1))
    raise RuntimeError("Unable to find version string.")


setup(
    name='django-custom-user',
    version=find_version('custom_user', '__init__.py'),
    license='Apache 2.0',

    install_requires=[],
    requires=[
        'Django (>=2.0)',
    ],

    description='Custom User Model',
    long_description=read('README.md'),

    author='Nicolas Lapalu',
    author_email='nicolas.lapalu@inrae.fr',

    url='https://forgemia.inra.fr/bioger/django-custom-user',
    download_url='https://forgemia.inra.fr/bioger/django-custom-user/-/archive/master/django-custom-user-master.zip',

    packages=find_packages(exclude=('example*',)),
    include_package_data=True,

    #test_suite = 'runtests',
    zip_safe=False,
    classifiers=[
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3',
        'Framework :: Django',
        'Framework :: Django :: 2.0',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
        'Topic :: Software Development :: Libraries :: Application Frameworks',
        'Topic :: Software Development :: Libraries :: Python Modules',
    ]
)
