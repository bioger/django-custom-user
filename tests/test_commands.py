from io import StringIO
from django.test import TestCase
from django.core.management import call_command

class CustomUserCommandsTests(TestCase):

    def setUp(self):
        pass

    def test_createuser(self):

        out = StringIO()
        call_command('createuser', 'test@test.fr', 'pass', stdout=out)
        expected = '\x1b[32;1mUser: test@test.fr successfully inserted\x1b[0m\n'
        self.assertEqual(expected, out.getvalue())
        out = StringIO()
        call_command('createuser', 'test@test.fr', 'pass', stdout=out)
        expected = '\x1b[31;1mUser: test@test.fr already exists, not inserted\x1b[0m\n'
        self.assertEqual(expected, out.getvalue())
        out = StringIO()
        call_command('createuser', 'test@test.fr.bademail', 'pass', stdout=out)
        expected = '\x1b[31;1memail "test@test.fr.bademail" not valid !\x1b[0m\n'
        self.assertEqual(expected, out.getvalue())

    def test_creategroup(self):

        out = StringIO()
        call_command('creategroup', 'mygroup', stdout=out)
        expected = '\x1b[32;1mGroup: mygroup successfully inserted\x1b[0m\n'
        self.assertEqual(expected, out.getvalue())
        out = StringIO()
        call_command('creategroup', 'mygroup', stdout=out)
        expected = '\x1b[31;1mGroup: mygroup already exists, not inserted\x1b[0m\n'
        self.assertEqual(expected, out.getvalue())

    def test_addusertogroup(self):
        
        out = StringIO()
        call_command('createuser', 'myuser@test.fr', 'pass', stdout=out)
        call_command('creategroup', 'mytestgroup', stdout=out)
        out = StringIO()
        call_command('addusertogroup', 'myuser@test.fr', 'mytestgroup', stdout=out)
        expected = '\x1b[32;1mUser: myuser@test.fr successfully inserted in Group: mytestgroup\x1b[0m\n'
        self.assertEqual(expected, out.getvalue())
        out = StringIO()
        call_command('addusertogroup', 'myuser@fake.fr', 'mytestgroup', stdout=out)
        expected = '\x1b[31;1mUser: myuser@fake.fr does not exist\x1b[0m\n'
        self.assertEqual(expected, out.getvalue())
        out = StringIO()
        call_command('addusertogroup', 'myuser@test.fr', 'myfakegroup', stdout=out)
        expected = '\x1b[31;1mGroup: myfakegroup does not exist\x1b[0m\n'
        self.assertEqual(expected, out.getvalue())

    def test_removeuserfromgroup(self):

        out = StringIO()
        call_command('createuser', 'myuser@test.fr', 'pass', stdout=out)
        call_command('creategroup', 'mytestgroup', stdout=out)
        out = StringIO()
        call_command('removeuserfromgroup', 'myuser@test.fr', 'mytestgroup', stdout=out)
        expected = '\x1b[32;1mUser: myuser@test.fr successfully removed from Group: mytestgroup\x1b[0m\n'
        self.assertEqual(expected, out.getvalue())
        out = StringIO()
        call_command('removeuserfromgroup', 'myuser@fake.fr', 'mytestgroup', stdout=out)
        expected = '\x1b[31;1mUser: myuser@fake.fr does not exist\x1b[0m\n'
        self.assertEqual(expected, out.getvalue())
        out = StringIO()
        call_command('removeuserfromgroup', 'myuser@test.fr', 'myfakegroup', stdout=out)
        expected = '\x1b[31;1mGroup: myfakegroup does not exist\x1b[0m\n'
        self.assertEqual(expected, out.getvalue())


