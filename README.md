# django-custom-user

Django app based on [custom-user-model](https://testdriven.io/blog/django-custom-user-model/) and [auth-customizing](https://docs.djangoproject.com/fr/3.0/topics/auth/customizing/)

## Installation

The custom authentication system implies modifications in the database and dependencies with other tables (groups, ...). So after the creation of your project DO NOT APPLY the migrations ! You must absolutely create the custom user model BEFORE you apply your first migration. Migrating to a custom model is not covered by this project (see: [blog](https://www.caktusgroup.com/blog/2013/08/07/migrating-custom-user-model-django/) and [django doc](https://docs.djangoproject.com/en/3.0/topics/auth/customizing/#substituting-a-custom-user-model)).

```python
# if public repo:
pip install git+https://git@forgemia.inra.fr/bioger/django-custom-user.git

# if private repo, set a token in project > settings > CI/CD > Deploy Tokens:
pip install git+https://<token-username>:<token:key>@forgemia.inra.fr/bioger/django-custom-user.git
```

## Configuration

Start your project:
```python
django-admin startproject mysite
```

Add to the settings:
```python
INSTALLED_APPS += (
    ...
    'custom_user'
)

AUTH_USER_MODEL = 'custom_user.CustomUser'
```

Run migrations:
```python
# OK run first migration (initiate db and run all migrations)
python manage.py migrate
```

Create a super user:
```python
python manage.py createsuperuser
```

Validate installation & admin site: 

connect to http://yoursite/admin  (or http://localhost:8000/admin if default) 

## Forms

Custom User app rewrites basic Auth forms. You can connect to the admin site with custom user model and email field as login. In case, you use another authentication backends (ex remote user, ldap, ...), you have to set default ModelBackend as second backend to allow connection of staff users.

```python
# ex: with Remote User Backend
AUTHENTICATION_BACKENDS = [
        'django.contrib.auth.backends.RemoteUserBackend',
        'django.contrib.auth.backends.ModelBackend',
        ]
```

## Subcommands

Custom User app is delivered with several subcommands to avoid the use of the admin site:

```python
# create a simple user
python manage.py createuser login@domain.fr password

# create a group
python manage.py creategroup mygroup

# add user to a group
python manage.py addusertogroup login@domain.fr mygroup

# remove user from a group
python manage.py removeuserfromgroup login@domain.fr mygroup
```
