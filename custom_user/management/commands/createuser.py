import sys
import re

from django.core.management.base import BaseCommand, CommandError

from django.contrib.auth import get_user_model 

class Command(BaseCommand):

    def add_arguments(self, parser):

        # positional args
        parser.add_argument('email', help='email', type=str)
        parser.add_argument('password', help='password', type=str)

        # optional args
        parser.add_argument('--staff', action='store_true', help='user is a staff member')

    def handle(self, *args, **options):

        # get user model (necessary with custom model)
        UserModel = get_user_model()

        if not self._is_valid_email(options['email']):
            self.stdout.write(self.style.ERROR("email \"{}\" not valid !".format(options['email'])))
            return

        try:
            if not UserModel.objects.filter(email=options['email']).exists():
                user = UserModel.objects.create_user(email=options['email'],password=options['password'])
                if options['staff']:
                    user.is_staff = True
                user.save()

                self.stdout.write(self.style.SUCCESS('User: {} successfully inserted'.format(options['email'])))
                return
            else:
                self.stdout.write(self.style.ERROR('User: {} already exists, not inserted'.format(options['email'])))
                return
        except Exception as e:
            raise CommandError("Something wrong happened, see log:\n {}".format(e))
        
    def _is_valid_email(self, email):

        return bool(re.match(r"^[\w\.\+\-]+\@([\w]+\.){1,4}[a-z]{2,3}$", email))


