from django.core.management.base import BaseCommand, CommandError

from django.contrib.auth.models import Group

class Command(BaseCommand):

    def add_arguments(self, parser):

        # positional args
        parser.add_argument('group', help='group name', type=str)

    def handle(self, *args, **options):

        try:
            if not Group.objects.filter(name=options['group']).exists():
                group = Group.objects.create(name=options['group'])
                group.save()

                self.stdout.write(self.style.SUCCESS('Group: {} successfully inserted'.format(options['group'])))
                return
            else:
                self.stdout.write(self.style.ERROR('Group: {} already exists, not inserted'.format(options['group'])))
                return
        except Exception as e:
            raise CommandError("Something wrong happened, see log:\n {}".format(e))
 
