from django.core.management.base import BaseCommand, CommandError

from django.contrib.auth import get_user_model 
from django.contrib.auth.models import Group

class Command(BaseCommand):

    def add_arguments(self, parser):

        # positional args
        parser.add_argument('email', help='email of the user', type=str)
        parser.add_argument('group', help='group name', type=str)

    def handle(self, *args, **options):

        try:
            UserModel = get_user_model()
            if not UserModel.objects.filter(email=options['email']).exists():
                self.stdout.write(self.style.ERROR('User: {} does not exist'.format(options['email'])))
                return
            if not Group.objects.filter(name=options['group']).exists():
                self.stdout.write(self.style.ERROR('Group: {} does not exist'.format(options['group'])))
                return

            user = UserModel.objects.get(email=options['email'])
            group = Group.objects.get(name=options['group'])
            group.user_set.add(user)
            group.save()

            self.stdout.write(self.style.SUCCESS('User: {} successfully inserted in Group: {}'.format(options['email'], options['group'])))
            return
        except Exception as e:
            raise CommandError("Something wrong happened, see log:\n {}".format(e))
 
